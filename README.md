Multiclass Hinge Loss. Implemented in Python using NumPy, PyTorch and Matplotlib library.

My lab assignment in Deep Learning, FER, Zagreb.

Task description in "TaskSpecification.pdf" in section "Bonus zadatak - Multiclass hinge loss (max 20%)".

Docs in "sadrzaj.txt"

Created: 2021

